<?php
namespace components;

use memento\Category as MementoCategory;

class App
{
    public $_people; // масив для хранения массива людей

    public $reference; // Переменная для хранения обьекта reference

    public $obj_people; // Переменная для хранения массива обьектов людей

    public function __construct()
    {
        echo 'Объем занимаемой памяти<br><br>';
        
        $start_memory = memory_get_usage(); // Обьем оперативной памяти выделяемой для нашего скрипта php в текущий момент времени        
        $start_time = microtime(true);      // Текущее время перед запуском герерации массива
        $this->_people = MementoCategory::get(); // Генерация массива 
        $finish_time = microtime(true); // Текущее время после герерации массива       
        $finish_memory = memory_get_usage(); // Обьем оперативной памяти выделяемой для нашего скрипта php в текущий момент времени милисекундах    
        
        $b_memory = $finish_memory - $start_memory; // Вычисление обьема выделеной оперативной памяти под сгенерированый массив в байтах
        $kb_memory = number_format($b_memory / 1024, 2); // тот же обьем только в килобайтах
        $mb_memory = number_format($b_memory / (1024 * 1024), 2); // тот же обьем только в мегабайтах
        $finish_time = $finish_time - $start_time; // Время за которое выполнилать операции генерации массива

        echo "$b_memory b<br>";  // Вывод оперативной памяти выделяемой для нашего массива $_people в байтах
        echo "$kb_memory kb<br>"; // Вывод оперативной памяти выделяемой для нашего массива $_people в килобайтах
        echo "$mb_memory mb<br><br>"; // Вывод оперативной памяти выделяемой для нашего массива $_people в мегабайтах
        
        echo 'Время выполнения скрипта<br><br>';
        echo $finish_time.' сек<br><br>'; // Вывод времени за которое выполнилать операции генерации массива в милисекундах


        //  вывод сгенерированого массива на экран $_people
        
        echo 'begin<br>';
        foreach ($this->_people as $key => $value) {
            echo "$key ";
            foreach ($value as $f_key => $f_value) {
               echo "$f_value ";
            }
            echo "<br>";
        }
        echo "<br>";


        // создание обьекта reference

        $this->reference = new Reference();

        // Передача масива $_people в свойство  set_temp_category обьекта reference

        $this->reference->set_temp_category($this->_people);

        $obj_start_memory = memory_get_usage();  // Обьем оперативной памяти выделяемой для нашего скрипта php в текущий момент времени      
        $obj_start_time = microtime(true);  // Текущее время перед запуском герерации массива обьетов

        $obj_people = $this->reference->getCategories(); // Генерация массива обьектов методом getCategories обьета reference

        $obj_finish_time = microtime(true);  // Обьем оперативной памяти выделяемой для нашего скрипта php в текущий момент времени       
        $obj_finish_memory = memory_get_usage();   // Текущее время после запуском герерации массива обьетов 

        $obj_b_memory = $obj_finish_memory - $obj_start_memory; // Вычисление обьема выделеной оперативной памяти под сгенерированый массив обьектов в байтах
        $obj_kb_memory = number_format($obj_b_memory / 1024, 2);    // тот же обьем только в килобайтах
        $obj_mb_memory = number_format($obj_b_memory / (1024 * 1024), 2);   // тот же обьем только в мегабайтах
        $obj_finish_time = $obj_finish_time - $obj_start_time;  // Время за которое выполнилать операции генерации массива

        echo "$obj_b_memory b<br>"; // Вывод оперативной памяти выделяемой для нашего массива обьектов $obj_people в байтах 
        echo "$obj_kb_memory kb<br>";   // Вывод оперативной памяти выделяемой для нашего массива обьектов $obj_people в килобайтах
        echo "$obj_mb_memory mb<br><br>"; // Вывод оперативной памяти выделяемой для нашего массива обьектов $obj_people в мегабайтах
        
        echo 'Время выполнения скрипта<br><br>';
        echo $obj_finish_time.' сек<br><br>';   // Вывод времени за которое выполнилать операции генерации массива обьектов в милисекундах

        //  вывод сгенерированого массива на экран $obj_people
        
        echo '<ul>';
        foreach ($obj_people as $category){
        echo '<li>'.$category->lastname.' '.$category->firstname.' '.$category->age.'</li>';
            
        }
        echo '</ul>';

    }


    /**
     * @return int
     */
    public function run()
    {
        
        echo 'run<br>';
        return 0;
    }

}
