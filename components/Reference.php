<?php
/**
 * Created by PhpStorm.
 * User: pudchdenis
 * Date: 20.08.16
 * Time: 15:53
 */

namespace components;

use models\Object;

class Reference
{
    protected $_categories = []; // Масcив обьектов

    protected $_temp_category = []; // массив массивов

    /**
     * @return array[]
    */
   
   // функция для создания масива обьетов

    public function getCategories()
    {
        if(empty($this->_categories)){
            foreach ($this->_temp_category as $params){
                $this->_categories[] = new Object($params['0'], $params[1], $params[2]);
            }
        }
        return $this->_categories;
    }

    // функция для передачи массива _people с обькта c класса App

    public function set_temp_category($array){
        $this->_temp_category = $array;
    }
}