<?php
namespace models;


class Object
{
    public $lastname;
    
    public $firstname;
    
    public $age;

    // функция конструктор для создания обьетов

    public function __construct($lastname, $firstname, $age)
    {
        $this->lastname = $lastname;
        $this->age = $age;
        $this->firstname = $firstname;
    }

    public function set_str() {
    	return $this->lastname." ".$this->firstname." ".$this->age; 
    }

}